-- Hint: use `:h <option>` to figure out the meaning if needed
vim.opt.clipboard = 'unnamedplus'   -- use system clipboard 
-- vim.opt.completeopt = {'menu', 'menuone', 'noselect'} -- completion options
vim.opt.mouse = 'a'                 -- allow the mouse to be used in Nvim

-- Tab
vim.opt.tabstop = 4                 -- number of visual spaces per TAB
vim.opt.softtabstop = 4             -- number of spacesin tab when editing
vim.opt.shiftwidth = 4              -- insert 4 spaces on a tab
vim.opt.expandtab = true            -- tabs are spaces, mainly because of python

-- UI config
-- vim.opt.number = true               -- show absolute number
-- vim.opt.relativenumber = true       -- add numbers to each line on the left side
-- vim.opt.cursorline = true           -- highlight cursor line underneath the cursor horizontally
vim.opt.splitbelow = true           -- open new vertical split bottom
vim.opt.splitright = true           -- open new horizontal splits right
-- vim.opt.termguicolors = true        -- enabl 24-bit RGB color in the TUI
vim.opt.showmode = false            -- we are experienced, wo don't need the "-- INSERT --" mode hint

-- Searching
vim.opt.incsearch = true            -- search as characters are entered
vim.opt.hlsearch = false            -- do not highlight matches
vim.opt.ignorecase = true           -- ignore case in searches by default
vim.opt.smartcase = true            -- but make it case sensitive if an uppercase is entered

-- Plugin manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",              -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Plugins
local plugins = {
    'neovim/nvim-lspconfig',
    {'nvim-orgmode/orgmode',
        dependencies = {
        { 'nvim-treesitter/nvim-treesitter', lazy = true },
          },
          event = 'VeryLazy',
          config = function()
            -- Load treesitter grammar for org
            require('orgmode').setup_ts_grammar()
        
            -- Setup treesitter
            require('nvim-treesitter.configs').setup({
              highlight = {
                enable = true,
                additional_vim_regex_highlighting = { 'org' },
              },
              ensure_installed = { 'org' },
            })
        
            -- Setup orgmode
            require('orgmode').setup({
              org_agenda_files = '~/org/*/*.org',
              org_default_notes_file = '~/org/pages/refile.org',
            })
          end,
    },
    "ms-jpq/coq_nvim",
    'ms-jpq/coq.artifacts',
    'ms-jpq/coq.thirdparty',
}

require("lazy").setup(plugins)

-- Binding
vim.g.mapleader = " "

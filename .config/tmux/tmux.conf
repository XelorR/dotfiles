# colors
set -g default-terminal "tmux-256color"
set -ag terminal-overrides ",xterm-256color:RGB"
set -g -a terminal-overrides ',*:Ss=\E[%p1%d q:Se=\E[2 q'

# bindings
unbind r
bind r source-file ~/.config/tmux/tmux.conf \; display "Reloaded"

bind -n C-h select-pane -L
bind -n C-j select-pane -D
bind -n C-k select-pane -U
bind -n C-l select-pane -R

bind q killp
#bind -n C-S-w killp
#bind -n C-S-t new-window
bind -n M-[ previous-window
bind -n M-] next-window
bind -n M-left previous-window
bind -n M-right next-window

bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "pbcopy"
bind v split-window -h
bind h split-window -v

# options
set -g history-limit 100000
set-window-option -g mode-keys vi
set -g mouse on
set-option -g status-position top

# plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

set -g @plugin 'catppuccin/tmux'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-fpp'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @plugin 'laktak/extrakto'

# ensure clonning TPM first:
# git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm
run '~/.config/tmux/plugins/tpm/tpm'

# My dotfiles

Saving various configurations I use on daily basis

## Rollout sctipts

Will git pull and copy all recent configs to it's places

- GNU Linux: [rollout_linux.sh](rollout_linux.sh)
- MacOS: [rollout_macos.sh](rollout_macos.sh)
- Termux: [rollout_termux.sh](rollout_termux.sh)
- Windows: [rollout_windows.ps1](rollout_windows.ps1)

## Tiling

- MacOS
  - [Yabai](https://github.com/koekeishiya/yabai) ([how to setup](yabai-install.md))
    - .config/yabai/[yabairc](.config/yabai/yabairc)
    - .config/skhd/[skhdrc](.config/skhd/skhdrc)
    - .config/spacebar/[spacebarrc](.config/spacebar/spacebarrc)
- GNU Linux
  - [Hyprland](https://hyprland.org)
    - .config/hypr/[hyprland](.config/hypr/hyprland.conf).conf
    - .config/[rofi](.config/rofi)
    - .config/[kitty](.config/kitty)
  - [Plasma](https://kde.org/plasma-desktop/) & [Bismuth](https://github.com/Bismuth-Forge/bismuth)
    - [configs](./plasma)
- Windows
  - [GlazeWM](https://github.com/glazerdesktop/GlazeWM)
    - [.glaze-wm/config.yaml](.glaze-wm/config.yaml)


## Terminal

- GNU Linux and MacOS - .config/fish/[config.fish](.config/fish/config.fish)
- GNU Linux and MacOS - [.bashrc | .zshrc](.bashrc) (works for both bash and zsh)
- GNU Linux and MacOS - [.config/kitty](.config/kitty)
- GNU Linux and MacOS - [.config/alacritty](.config/alacritty)
- GNU Linux - [.local/share/konsole](.local/share/konsole)
- Windows - [powershell](WindowsPowerShell/profile.ps1) - bash-like bindings
- Android - [termux](.termux)

## Better modifiers


**GNU Linux and Windows**
![](assets/qmk_pc.png)

**MacOS**
![](assets/qmk_mac.png)

**QMK Via/Vial**

- `MT(MOD_LCTL, KC_ESC)`: CAPS as both CONTROL and ESCAPE
- `MT(MOD_LALT, KC_SPC)`: SPACE as ALT on hold
- `MT(MOD_RCTL, KC_LALT)`: (_PC layout only_) LEFT ALT as COMMAND --- simulating macos-like Command+letter shortcuts
- `MT(MOD_RALT, KC_CAPS)`: RIGHT ALT to switch layout on tap (usually mapped on CAPS)
- `MT(MOD_RCMD, KC_CAPS)`: (_MacOS layout only_) RIGHT COMMAND to switch layout on tap (usually mapped on CAPS)
- `MT(MOD_RSFT, KC_ENT)`: ENTER as RIGHT SHIFT on hold --- sometimes I accidentaly hold it instead of RIGHT SHIFT
- `MT(MOD_LCTL | MOD_LSFT | MOD_LALT | MOD_LGUI,KC_NO)`: (_MacOS layout only_) LEFT OPT AS HYPER --- to bind WIN+L like lockscreen

**Software simulation**

- GNU Linux: [xremap config](.config/xremap/better_modifiers.yml)
- Windows: [autohotkey config](caps_remap_windows/better_modifiers.ahk)

**I also have following additional software remappings to ensure similar experience on all OSes:**

MacOS (karabiner, from PC-Style collection):
![](assets/karabiner.png)

Windows (autohotkey) - [caps to switch keyboard layout](caps_remap_windows/caps_switch_layout.ahk) (no native setting on windows)

## More on better CapsLock (I don't use it right now, currently I most of my remapping needs are done with QMK)

- Escape on tap, Control on hold:
  - MacOS - [karabiner rule](https://ke-complex-modifications.pqrs.org/#caps_lock) (not mine, just saving link which works for me)
  - Linux - [Xremap](https://github.com/k0kubun/xremap) config
    - .config/xremap/[caps_ctrl-esc.yml](.config/xremap/caps_ctrl-esc.yml)
    - /lib/systemd/system/[supercaps.service](lib/systemd/system/supercaps.service) - systemd service for above
  - Windows - [AutoHotkey v2](https://www.autohotkey.com) script
    - [caps\_is\_ctrl-esc.ahk](caps_remap_windows/caps_is_ctrl-esc.ahk)

## Emacs

- [SpaceMacs](https://www.spacemacs.org) config
  - [.spacemacs](.spacemacs)
- [.config/systemd/user/emacs.service](.config/systemd/user/emacs.service)

## Installed packages

### Linux - Manjaro packages

- [shell script installer](manjaro_install_software.sh)


## Other OS-specific settings

### MacOS - instantly show dock on mouse hover

Execute once
```bash
defaults write com.apple.dock "autohide-delay" -float "0" && killall Dock
```

### Windows - mac-like bindings (currently unused)

- [macos/logseq/emacs - like editing bindings for w11](w11.ahk) - run with [autohotkey](https://www.autohotkey.com) v2

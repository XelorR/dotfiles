Set-PSReadLineOption -EditMode Emacs
if (Get-Command -ErrorAction SilentlyContinue starship) {Invoke-Expression (&starship init powershell)}
if (Get-Command -ErrorAction SilentlyContinue lfcd) {Set-Alias -Name lf -Value lfcd}


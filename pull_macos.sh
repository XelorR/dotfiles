#!/usr/bin/env sh

cp ~/.config/yabai/yabairc .config/yabai/yabairc
cp ~/.config/skhd/skhdrc .config/skhd/skhdrc
cp ~/.config/spacebar/spacebarrc .config/spacebar/spacebarrc
cp ~/.spacemacs .spacemacs


#!/usr/bin/env bash

git pull

mkdir -p ~/.config/{kitty,rofi,hypr,systemd,xremap,fish,tmux}
mkdir -p ~/.config/systemd/user
mkdir -p ~/.config/alacritty/catppuccin
mkdir -p ~/.local/share/konsole/

# ignore next TWO lines if your resolution is less than 1080p
cp .spacemacs ~/.spacemacs
cp .config/kitty/kitty.conf ~/.config/kitty/
cp .config/kitty/tokyonight_night.conf ~/.config/kitty/

cp .config/alacritty/catppuccin/* ~/.config/alacritty/catppuccin/
if [ $DESKTOP_SESSION == plasma ]; then
	cat .config/alacritty/alacritty.yml | sed 's/decorations: none/decorations: full/' >~/.config/alacritty/alacritty.yml
else
	cp .config/alacritty/alacritty.yml ~/.config/alacritty/
fi

cp .local/share/konsole/* ~/.local/share/konsole/

cp .config/systemd/user/*.service ~/.config/systemd/user/
cp .config/xremap/*.yml ~/.config/xremap/
cat .config/xremap/{better_modifiers,macos-like_tabs}.yml >~/.config/xremap/config.yml

cp .config/hypr/hyprland.conf ~/.config/hypr/hyprland.conf
cp .config/rofi/* ~/.config/rofi/

if ! test -f ~/.config/tmux/plugins/tpm/tpm; then
	git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm
fi
cp .config/tmux/tmux.conf ~/.config/tmux/

cp .bashrc ~/.zshrc
cp .bashrc ~/.bashrc
cp .config/fish/config.fish ~/.config/fish/

if test -f "$HOME/.config/nvim/lua/config/lazy.lua"; then
	cp .config/nvim/lua/config/* ~/.config/nvim/lua/config/
	cp .config/nvim/lua/plugins/* ~/.config/nvim/lua/plugins/
else
	# required
	mv ~/.config/nvim{,.bak}

	# optional but recommended
	mv ~/.local/share/nvim{,.bak}
	mv ~/.local/state/nvim{,.bak}
	mv ~/.cache/nvim{,.bak}

	git clone https://github.com/LazyVim/starter ~/.config/nvim
	cp .config/nvim/lua/config/* ~/.config/nvim/lua/config/
	cp .config/nvim/lua/plugins/* ~/.config/nvim/lua/plugins/
fi

echo "Done. Latest versions of config files copied to current machine."

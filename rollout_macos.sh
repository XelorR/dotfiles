#!/usr/bin/env bash

git pull

mkdir -p ~/.config/{yabai,skhd,spacebar,kitty,tmux}
mkdir -p ~/.config/alacritty/catppuccin

# ignore next TWO lines if your resolution is less than 1080p
cp .spacemacs ~/.spacemacs
cp .config/kitty/kitty.conf ~/.config/kitty/
cp .config/kitty/tokyonight_night.conf ~/.config/kitty/

cp .config/alacritty/alacritty.yml ~/.config/alacritty/
cp .config/alacritty/catppuccin/* ~/.config/alacritty/catppuccin/
sed -i '' 's/size: 9/size: 13/' ~/.config/alacritty/alacritty.yml

cp .config/yabai/yabairc ~/.config/yabai/
cp .config/skhd/skhdrc ~/.config/skhd/
cp .config/spacebar/spacebarrc ~/.config/spacebar/

cp .bashrc ~/.zshrc
cp .bashrc ~/.bashrc
cp .config/fish/config.fish ~/.config/fish/

if ! test -f ~/.config/tmux/plugins/tpm/tpm; then
	git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm
fi
cp .config/tmux/tmux.conf ~/.config/tmux/

if test -f "$HOME/.config/nvim/lua/config/lazy.lua"; then
	cp .config/nvim/lua/config/* ~/.config/nvim/lua/config/
	cp .config/nvim/lua/plugins/* ~/.config/nvim/lua/plugins/
else
	# required
	mv ~/.config/nvim{,.bak}

	# optional but recommended
	mv ~/.local/share/nvim{,.bak}
	mv ~/.local/state/nvim{,.bak}
	mv ~/.cache/nvim{,.bak}

	git clone https://github.com/LazyVim/starter ~/.config/nvim
	cp .config/nvim/lua/config/* ~/.config/nvim/lua/config/
	cp .config/nvim/lua/plugins/* ~/.config/nvim/lua/plugins/
fi

defaults write com.apple.dock "autohide-delay" -float "0" && killall Dock

echo "Done. Latest versions of config files copied to current machine."
sudo yabai --load-sa

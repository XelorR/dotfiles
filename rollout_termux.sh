#!/usr/bin/env bash

git pull

cp .spacemacs ~/.spacemacs
cp .bashrc ~/.zshrc
cp .bashrc ~/.bashrc

if test -f "$HOME/.config/nvim/lua/config/lazy.lua"; then
	cp .config/nvim/lua/config/* ~/.config/nvim/lua/config/
	cp .config/nvim/lua/plugins/* ~/.config/nvim/lua/plugins/
else
	# required
	mv ~/.config/nvim{,.bak}

	# optional but recommended
	mv ~/.local/share/nvim{,.bak}
	mv ~/.local/state/nvim{,.bak}
	mv ~/.cache/nvim{,.bak}

	git clone https://github.com/LazyVim/starter ~/.config/nvim
	cp .config/nvim/lua/config/* ~/.config/nvim/lua/config/
	cp .config/nvim/lua/plugins/* ~/.config/nvim/lua/plugins/
fi

cp .termux/* ~/.termux/
termux-reload-settings

echo "Done. Latest versions of config files copied to current machine."

git pull

if (-Not (Test-Path -Path $env:USERPROFILE\AppData\Local\nvim)) {
    git clone https://github.com/LazyVim/starter $env:USERPROFILE\AppData\Local\nvim
}

if (-Not (Test-Path -Path $env:USERPROFILE\AppData\Roaming\.emacs.d)) {
    git clone https://github.com/syl20bnr/spacemacs $env:USERPROFILE\AppData\Roaming\.emacs.d
}

cp .\.config\nvim\lua\config\* $env:USERPROFILE\AppData\Local\nvim\lua\config\
cp .\.config\nvim\lua\plugins\* $env:USERPROFILE\AppData\Local\nvim\lua\plugins\
cp .\.spacemacs $env:USERPROFILE\AppData\Roaming\

$docs = [Environment]::GetFolderPath("MyDocuments")
mkdir -Force "$docs\WindowsPowerShell"
mkdir -Force "$env:USERPROFILE\.glaze-wm"

cp .\WindowsPowerShell\profile.ps1 "$docs\WindowsPowerShell\"
cp .\.glaze-wm\config.yaml $env:USERPROFILE\.glaze-wm\

echo "Done. Latest versions of config files copied to current machine."


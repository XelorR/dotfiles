GroupAdd "system", "ahk_class #32770" ; Win+R
GroupAdd "system", "ahk_class ApplicationFrameWindow ahk_exe ApplicationFrameHost.exe" ; Win11 Clock app and settings window
GroupAdd "system", "ahk_class Windows.UI.Core.CoreWindow ahk_exe SearchHost.exe" ; Win+s, Spotlight
GroupAdd "system", "ahk_class XamlExplorerHostIslandWindow ahk_exe explorer.exe" ; Win+Tab, MissionControl
GroupAdd "system", "ahk_class Shell_TrayWnd ahk_exe explorer.exe" ; panel/dock
GroupAdd "system", "ahk_exe CredentialUIBroker.exe" ; credentials request
GroupAdd "system", "ahk_class WinUIDesktopWin32WindowClass" ; used at least by PowerToys
GroupAdd "system", "ahk_class #32770" ; save dialog

GroupAdd "browsers", "ahk_exe brave.exe"
GroupAdd "browsers", "ahk_exe chrome.exe"
GroupAdd "browsers", "ahk_exe msedge.exe"
GroupAdd "browsers", "ahk_exe firefox.exe"
GroupAdd "browsers", "ahk_exe vivaldi.exe"
GroupAdd "programs", "ahk_group browsers"

GroupAdd "programs", "ahk_exe pycharm64.exe"
GroupAdd "programs", "ahk_exe idea.exe"
GroupAdd "programs", "ahk_exe VSCodium.exe"
GroupAdd "programs", "ahk_exe Code.exe"
GroupAdd "programs", "ahk_exe Miro.exe"
GroupAdd "programs", "ahk_exe KeePass.exe"
GroupAdd "programs", "ahk_exe KeePassXC.exe"
GroupAdd "programs", "ahk_exe Postman.exe"
GroupAdd "programs", "ahk_exe webexhost.exe"
GroupAdd "programs", "ahk_exe atmgr.exe"
GroupAdd "programs", "ahk_exe ciscocollabhost.exe"
GroupAdd "programs", "ahk_exe wmlhost.exe"
GroupAdd "programs", "ahk_class Notepad ahk_exe notepad.exe"
GroupAdd "programs", "ahk_exe One Photo Viewer.exe"
GroupAdd "programs", "ahk_exe SCNotification.exe"
GroupAdd "programs", "ahk_exe flameshot.exe"
GroupAdd "programs", "ahk_exe PowerToys.PowerLauncher.exe"

GroupAdd "office", "ahk_exe outlook.exe"
GroupAdd "office", "ahk_exe excel.exe"
GroupAdd "office", "ahk_exe winword.exe"
GroupAdd "office", "ahk_exe powerpnt.exe"
GroupAdd "office", "ahk_exe teams.exe"
GroupAdd "office", "ahk_exe onenote.exe"
GroupAdd "office", "ahk_exe onedrive.exe"
GroupAdd "programs", "ahk_group office"

GroupAdd "explorer", "ahk_class CabinetWClass ahk_exe explorer.exe" ; file browser
GroupAdd "explorer", "ahk_class WorkerW ahk_exe explorer.exe" ; desktop

GroupAdd "everything", "ahk_group system"
GroupAdd "everything", "ahk_group explorer"
GroupAdd "everything", "ahk_group programs"

^!q::DllCall("LockWorkStation") ; MacOS-like lock screen
!+x::PrintScreen

#HotIf WinActive("ahk_group everything")
{
    ; Emacs and LogSeq-like edition and navigation
    !p::Up
    !n::Down
    !f::^Right
    !b::^Left
    !a::Home
    !e::End
    !k::Send "{Shift Down}{End}{Shift Up}{Del}"
    !u::Send "{Shift Down}{Home}{Shift Up}{Backspace}"
    !Left::Home
    !Right::End
    !Backspace::Send "{Shift Down}{Home}{Shift Up}{Backspace}"
    !Delete::Send "{Shift Down}{End}{Shift Up}{Del}"
    !d::^Delete
    !w::^Backspace
    !l::Send "{Home}{Shift Down}{End}{Shift Up}{Del}"
    !z::^z
    !x::^x
    !c::^c
    !v::^v

    !+,::^Home
    !+.::^End

    ; Ensuring that combinations with Shift is keeping default behaviour
    !+p::!+p
    !+n::!+n
    !+f::!+f
    !+b::!+b
    !+a::!+a
    !+e::!+e
    !+k::!+k
    !+u::!+u
    !+Left::!+Left
    !+Right::!+Right
    ^!Left::^!Left
    ^!Right::^!Right
    !+Backspace::!+Backspace
    !+Delete::!+Delete
    !+d::!+d
    !+w::!+w
    !+l::!+l
    !+z::^y
    !+x::!+x
    !+c::!+c
    !+v::!+v

    ; tabs and history
    !+sc01B::^pgdn
    !+sc01A::^pgup
    !sc01B::!right
    !sc01A::!left
}

#HotIf WinActive("ahk_exe WindowsTerminal.exe")
{
    !+sc01B::^tab
    !+sc01A::^+tab
    ^pgdn::^tab
    ^pgup::^+tab
}